#!/bin/sh

echo "Preparing:"
set -x                          # Output commands
set -e                          # Abort on errors

echo "Checking:"
pwd
hostname
date

echo "Environment:"
export CACTUS_NUM_PROCS=@NUM_PROCS@
export CACTUS_NUM_THREADS=@NUM_THREADS@
export OMP_NUM_THREADS=@NUM_THREADS@
#env | sort > SIMFACTORY/ENVIRONMENT

echo "Job setup:"
echo "   Allocated:"
echo "      Nodes:                      @NODES@"
echo "      Cores per node:             @PPN@"
echo "   Running:"
echo "      MPI processes:              @NUM_PROCS@"
echo "      OpenMP threads per process: @NUM_THREADS@"
echo "      MPI processes per node:     @(1.0*@NUM_PROCS@/@NODES@)@"
echo "      OpenMP threads per core:    @(1.0*(@NUM_PROCS@*@NUM_THREADS@)/(@NODES@*@PPN@))@"
#echo "      OpenMP threads per node:    @PPN_USED@"

echo "Starting:"
export CACTUS_STARTTIME=$(date +%s)

if [ @RUNDEBUG@ -eq 0 ]; then
    # TODO: check if one needs to manually prune the hostfile so that running
    # with 1 taks per node works otherwise OpenMPI is sometimes picky with
    # ranks and sockets
    mpirun -np @NUM_PROCS@ -cpus-per-rank @NUM_THREADS@ @EXECUTABLE@ -L 3 @PARFILE@
else
    gdb --args @EXECUTABLE@ -L 3 @PARFILE@
fi

echo "Stopping:"
date
